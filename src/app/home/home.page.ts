import { Component } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  myres: any;
  user: any;
  google: any;
  constructor (private fb: Facebook, private googlePlus: GooglePlus) { }
  facebok() {
    this.fb.login(['public_profile', 'user_friends', 'email', 'user_photos', 'user_gender'])
      .then((res: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', res)
        this.myres = res;
        this.getUserDetail(res.authResponse.userID);
      }

      )
      .catch(e => console.log('Error logging into Facebook', e));

    this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);

  }


  getUserDetail(userId) {
    this.fb.api('/' + userId + '/?fields=id,email,name,picture,gender', ['public_profile', 'user_photos', 'user_gender', 'user_friends'])
      .then((detail) => {
        console.log('User detail: ', detail);
        this.user = detail;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  fngoogle() {
    this.googlePlus.login({
      webClientId: '133733970120-jsg3qe7jncj6ggnlvjb6hhbkj20g5kdi.apps.googleusercontent.com'
    })
      .then(res => {
        console.log('estos son los datos devuelto');
        console.log(res)
        this.google = res;
      })
      .catch(err => {
        alert(JSON.stringify(err));
        console.log('este es un error')
        console.log(err);
        console.error(err)
      }

      );
  }
}
